package com.example.artspace

import android.graphics.BlurMaskFilter
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ElevatedButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.ui.graphics.Paint
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.drawscope.drawIntoCanvas
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.artspace.ui.theme.ArtSpaceTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ArtSpaceTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    ArtSpaceScreen()
                }
            }
        }
    }
}

@Composable
fun ArtSpaceScreen(modifier: Modifier = Modifier) {

    var index by remember { mutableStateOf(0) }

    fun restart() {
        index = 0
    }

    fun objResource(title: Int, year: Int, image: Int): Map<String, Int> {
        val reso = mapOf("title" to title, "year" to year, "image" to image)
        return reso
    }

    var meLaSudanTodos = arrayOf(
        objResource(R.string.shiro, R.string.shiro_year, R.drawable.shiro),
        objResource(R.string.akame, R.string.akame_year, R.drawable.akame),
        objResource(R.string.cheems, R.string.cheems_year, R.drawable.cheems),
        objResource(R.string.franklin, R.string.franklin_year, R.drawable.franklin),
        objResource(R.string.gojo, R.string.gojo_year, R.drawable.gojo),
        objResource(R.string.midoriya, R.string.midoriya_year, R.drawable.midoriya),
        objResource(R.string.mitsuha, R.string.mitsuha_year, R.drawable.mitsuha),
        objResource(R.string.shinchan, R.string.shinchan_year, R.drawable.shinchan),
        objResource(R.string.nero, R.string.nero_year, R.drawable.nero),
        objResource(R.string.tatsumi, R.string.tatsumi_year, R.drawable.tatsumi),
        )


    Column(
        modifier = modifier.fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally,
    ){
        Text(
            text = "1841391 - Santiago Ramirez",
            color = colorResource(id = R.color.purple_500),
            fontWeight = FontWeight.Bold)

        meLaSudanTodos[index].get("image")?.let {
            ArtworkImage(
                currentArtwork = it
            )
        }
        Spacer(
            modifier = modifier.size(12.dp)
        )

        ArtworkTitle(
            title = meLaSudanTodos[index].get("title")!!,
            year = meLaSudanTodos[index].get("year")!!,
        )
        Spacer(
            modifier = modifier.size(25.dp)
        )
        Row(
            modifier = modifier
                .padding(horizontal = 6.dp)
                .fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            ElevatedButton(
                onClick = {
                    /*when (currentArtwork) {
                        firstArtwork -> {
                            currentArtwork = fourthArtwork
                            title = R.string.naruto
                            year = R.string.naruto_year
                        }
                        secondArtwork -> {
                            currentArtwork = firstArtwork
                            title = R.string.denji
                            year = R.string.denji_year
                        }
                        thirdArtwork -> {
                            currentArtwork = thirdArtwork
                            title = R.string.zero_two
                            year = R.string.zero_two_year
                        }
                        else -> {
                            currentArtwork = thirdArtwork
                            title = R.string.sanji
                            year = R.string.sanji_year
                        }
                    }*/
                      if (index == (meLaSudanTodos.size-1)) {
                          index = 0
                      } else {
                          index += 1
                      }
                },
               colors = ButtonDefaults.elevatedButtonColors(
                   containerColor = colorResource(R.color.prev_color)
               )

            ) {
                Text(
                    text = stringResource(R.string.previous),
                    fontSize = 16.sp,
                    fontWeight = FontWeight.Medium,
                    color = colorResource(id = R.color.white)
                )
            }

            ElevatedButton(
                onClick = { restart()},
                colors = ButtonDefaults.elevatedButtonColors(
                    containerColor = colorResource(id = R.color.restart)
                )) {
                Column(modifier = Modifier.padding(2.dp)) {
                    Image(
                        painter = painterResource(R.drawable.icon),
                        contentDescription = "xd",
                        modifier = Modifier
                            .height(30.dp)
                            .width(30.dp))
                }
            }

            ElevatedButton(
                onClick = {
                    /*when (currentArtwork) {
                        firstArtwork -> {
                            currentArtwork = secondArtwork
                            title = R.string.zero_two
                            year = R.string.zero_two_year
                        }
                        secondArtwork -> {
                            currentArtwork = thirdArtwork
                            title = R.string.sanji
                            year = R.string.sanji_year
                        }
                        thirdArtwork -> {
                            currentArtwork = fourthArtwork
                            title = R.string.naruto
                            year = R.string.naruto_year
                        }
                        else -> {
                            currentArtwork = firstArtwork
                            title = R.string.denji
                            year = R.string.denji_year
                        }
                    }*/
                    if (index == 0) {
                        index = meLaSudanTodos.size - 1
                    } else {
                        index -= 1
                    }
                },
                colors = ButtonDefaults.elevatedButtonColors(
                    containerColor = colorResource(R.color.next_color)
                )
            ) {

                Text(
                    text = stringResource(R.string.next),
                    fontSize = 16.sp,
                    fontWeight = FontWeight.Medium,
                    color = colorResource(id = R.color.white)
                )
            }
        }
    }
}

@Composable
fun ArtworkImage(
    modifier: Modifier = Modifier
        .padding(
            start = 60.dp,
            top = 15.dp,
            end= 60.dp,
            bottom = 40.dp
        )
        .shadow(
            borderRadius = 12.dp,
            blurRadius = 60.dp,
        ),
    @DrawableRes currentArtwork: Int
) {
    Image(
        painter = painterResource(id = currentArtwork),
        contentDescription = null,
        modifier = modifier
            .height(500.dp)
            .width(300.dp),
        contentScale = ContentScale.FillWidth
    )
}

@Composable
fun ArtworkTitle(
    @StringRes title: Int,
    @StringRes year: Int,
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = stringResource(id = title),
            fontWeight = FontWeight.Bold,
            color = colorResource(id = R.color.blue_100),
            fontSize = 32.sp,
        )
        Text(
            text = stringResource(id = year),
            fontWeight = FontWeight.Medium,
            color = colorResource(id = R.color.gray_300),
            fontSize = 16.sp,
        )
    }
}

fun Modifier.shadow(
    color: Color = Color.Cyan,
    borderRadius: Dp = 0.dp,
    blurRadius: Dp = 0.dp,
    offsetY: Dp = 0.dp,
    offsetX: Dp = 0.dp,
    spread: Dp = 0f.dp,
    modifier: Modifier = Modifier
) = this.then(
    modifier.drawBehind {
        this.drawIntoCanvas {
            val paint = Paint()
            val frameworkPaint = paint.asFrameworkPaint()
            val spreadPixel = spread.toPx()
            val leftPixel = (0f - spreadPixel) + offsetX.toPx()
            val topPixel = (0f - spreadPixel) + offsetY.toPx()
            val rightPixel = (this.size.width + spreadPixel)
            val bottomPixel = (this.size.height + spreadPixel)

            if (blurRadius != 0.dp) {
                frameworkPaint.maskFilter =
                    (BlurMaskFilter(blurRadius.toPx(), BlurMaskFilter.Blur.NORMAL))
            }

            frameworkPaint.color = color.toArgb()
            it.drawRoundRect(
                left = leftPixel,
                top = topPixel,
                right = rightPixel,
                bottom = bottomPixel,
                radiusX = borderRadius.toPx(),
                radiusY = borderRadius.toPx(),
                paint
            )
        }
    }
)

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    ArtSpaceTheme {
        ArtSpaceScreen()
    }
}
